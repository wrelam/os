CC=i686-elf-gcc
AS=i686-elf-as
LD=i686-elf-ld

all: bootloader kernel os iso

bootloader:
	$(AS) boot.s -o boot.o

kernel:
	$(CC) -c kernel.c -o kernel.o -std=gnu99 -ffreestanding -O2 -Wall -Wextra

os: bootloader kernel
	$(CC) -T linker.ld -o myos.bin -ffreestanding -O2 -nostdlib boot.o kernel.o -lgcc

iso: clean os
	mkdir -p isodir/boot/grub
	cp myos.bin isodir/boot/
	cp grub.cfg isodir/boot/grub/
	grub2-mkrescue -o myos.iso isodir

clean:
	rm -rf *.o *.bin *.iso isodir
