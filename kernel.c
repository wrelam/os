/*******************************************************************************
 * kernel.c
 *
 * Initial kernel functions
*******************************************************************************/

#ifndef __cplusplus
#include <stdbool.h>
#endif
#include <stddef.h>
#include <stdint.h>

#ifdef __linux__
#error "You don't seem to be using the cross-compiler!"
#endif

#ifndef __i386__
#error "This needs to be compiled with an i686-elf compiler"
#endif

/* Macros */
#define termPos(x,y)    (((y) * VGA_WIDTH) + (x))

/* Hardware text mode color constants */
typedef enum vgaColor
{
    VGACOLOR_BLACK          = 0,
    VGACOLOR_BLUE           = 1,
    VGACOLOR_GREEN          = 2,
    VGACOLOR_CYAN           = 3,
    VGACOLOR_RED            = 4,
    VGACOLOR_MAGENTA        = 5,
    VGACOLOR_BROWN          = 6,
    VGACOLOR_LIGHT_GREY     = 7,
    VGACOLOR_DARK_GREY      = 8,
    VGACOLOR_LIGHT_BLUE     = 9,
    VGACOLOR_LIGHT_GREEN    = 10,
    VGACOLOR_LIGHT_CYAN     = 11,
    VGACOLOR_LIGHT_RED      = 12,
    VGACOLOR_LIGHT_MAGENTA  = 13,
    VGACOLOR_LIGHT_BROWN    = 14,
    VGACOLOR_WHITE          = 15,
} vgaColor;

/* Globals */
static const size_t VGA_WIDTH   = 80;
static const size_t VGA_HEIGHT  = 25;

size_t terminalColumn       = 0;
size_t terminalRow          = 0;
uint8_t terminalColor       = 0;
uint16_t *terminalBuffer    = NULL;

/* Functions */
uint8_t
make_color(const vgaColor fg, const vgaColor bg)
{
    return ((uint8_t) fg) | (((uint8_t) bg) << 4);
}

uint16_t
make_vgaentry(const char c, const uint8_t color)
{
    return ((uint16_t) c) | (((uint16_t) color) << 8);
}

size_t
strlen(const char *str)
{
    register size_t ret = 0;
    while (str[++ret]) ;
    return ret;
}

void
terminal_initialize(void)
{
    register size_t x   = 0;
    register size_t y   = 0;

    terminalRow = 0;
    terminalColumn = 0;
    terminalColor = make_color(VGACOLOR_GREEN, VGACOLOR_BLACK);
    terminalBuffer = (uint16_t *) 0xB8000;

    for (y = 0; y < VGA_HEIGHT; y++)
    {
        for (x = 0; x < VGA_WIDTH; x++)
        {
            terminalBuffer[termPos(x,y)] = make_vgaentry(' ', terminalColor);
        }
    }
}

void
terminal_setcolor(const uint8_t color)
{
    terminalColor = color;
}

void
terminal_putentryat(
    const char      c,
    const uint8_t   color,
    const size_t    x,
    const size_t    y)
{
    terminalBuffer[termPos(x,y)] = make_vgaentry(c, color);
}

void
terminal_scroll(void)
{
    register size_t x   = 0;
    register size_t y   = 0;

    /* Check to see if the next write will be out of bounds */
    if (VGA_WIDTH <= terminalColumn)
    {
        /* Next print will be on the next row */
        terminalRow++;
        terminalColumn = 0;
    }

    /* Scroll all lines up if next row is off the display */
    if (VGA_HEIGHT <= terminalRow)
    {
        terminalRow = VGA_HEIGHT - 1;

        /* Shift all rows up by one */
        for (y = 1; y < VGA_HEIGHT; y++)
        {
            for (x = 0; x < VGA_WIDTH; x++)
            {
                terminalBuffer[termPos(x,y-1)] = 
                                            terminalBuffer[termPos(x,y)];
            }
        }

        /* Clear last row for incoming print */
        y = VGA_HEIGHT - 1;
        for (x = 0; x < VGA_WIDTH; x++)
        {
            terminalBuffer[termPos(x,y)] = 
                                        make_vgaentry(' ', terminalColor);
        }
    }
}

void
terminal_putchar(const char c)
{
    /* Attempt to scroll first since we know we are about to print something */
    terminal_scroll();

    switch (c)
    {
    case '\n':
        terminalRow++;
        terminalColumn = 0;
        break;
    default:
        terminal_putentryat(c, terminalColor, terminalColumn, terminalRow);
        terminalColumn++;
        break;
    }
}

void
terminal_writestring(const char *data)
{
    register size_t i = 0;

    for (i = 0; i < strlen(data); i++)
    {
        terminal_putchar(data[i]);
    }
}

#ifdef __cplusplus
extern "C"
#endif
void
kernel_main(void)
{
    terminal_initialize();

    terminal_writestring("Hello, Kernel World!\n");
    terminal_writestring("1\n");
    terminal_writestring("2\n");
    terminal_writestring("3\n");
    terminal_writestring("4\n");
    terminal_writestring("5\n");
    terminal_writestring("6\n");
    terminal_writestring("7\n");
    terminal_writestring("8\n");
    terminal_writestring("9\n");
    terminal_writestring("10\n");
    terminal_writestring("11\n");
    terminal_writestring("12\n");
    terminal_writestring("13\n");
    terminal_writestring("14\n");
    terminal_writestring("15\n");
    terminal_writestring("16\n");
    terminal_writestring("17\n");
    terminal_writestring("18\n");
    terminal_writestring("19\n");
    terminal_writestring("20\n");
    terminal_writestring("21\n");
    terminal_writestring("22\n");
    terminal_writestring("23\n");
    terminal_writestring("24\n");
    terminal_writestring("25\n");
    terminal_writestring("26\n");
    terminal_writestring("27\n");
    terminal_writestring("28\n");
    terminal_writestring("29\n");
    terminal_writestring("30\n");
    terminal_writestring("Hello, Kernel World!\n");
}

