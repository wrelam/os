# Declare constants used for creating a multiboot header.

.set ALIGN,     1<<0                # Align modules on page boundaries
.set MEMINFO,   1<<1                # Memory map
.set FLAGS,     ALIGN | MEMINFO     # Multiboot "flag" field
.set MAGIC,     0x1BADB002          # Let's bootloader find the header
.set CHECKSUM,  -(MAGIC + FLAGS)    # Checksum of above, proves we are multiboot

# Declare the Multiboot Standard header.
.section .multiboot
.align 4
.long MAGIC
.long FLAGS
.long CHECKSUM

# Establish our own stack
.section .bootstrap_stack, "aw", @nobits
stack_bottom:
.skip 16384     # 16 KB
stack_top:

# Entry point to the kernel, no need to return since the bootloader will be gone
.section .text
.global _start
.type _start, @function
_start:

    # Point ESP to the top of our stack so it grows downwards
    movl $stack_top, %esp

    # Execute kernel_main which is written in C
    call kernel_main

    # Infinitely loop by disabling interrupts, halting, and jumping endlessly if
    # execution continues
    cli
    hlt

# Infinite loop
.Lhang:
    jmp .Lhang

# Determine the number of bytes from the _start symbol to here. Useful for
# debugging or a later implementation of call tracing
.size _start, . - _start
    
